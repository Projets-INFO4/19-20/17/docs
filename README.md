# LogGPX - info4 2019-2020
The goal of this project is to develop an Android application targeting hikers.
Its name is LogGPX and it will allow the VisuGPX company and its users to generate GPX files that can be used, analysed and exploited by PWA trace managers.
This application should be able to record traces on field without using a network, showing a real time map and should also be able to synchronize the data with the user's account if he logs in. 


<<<<<<< HEAD
## Présentation générale du projet

Le projet consiste à développer une application Android destinée aux adeptes de la randonnée : LogGPX. Cette application permettra à l’entreprise VisuGPX et plus particulièrement à ses clients de générer des fichiers GPX exploitables par des PWA de gestion de trace, dont VisuGPX.com fait parti. L’appli devra donc permettre l’enregistrement de traces sur le terrain hors couverture réseau, avec l’affichage d’une cartographie en temps réel, et la synchronisation, si l'utiliateur dispose d'un compte, des traces enregistrées sur VisuGPX.

## Journal de bord
=======

## Présentation générale du projet

## Log Book

### January 20th

During this class, we didn't do much because the only thing that we did was choosing the LogGPX topic. Then, we discussed a bit about what IDE we should use and we quickly though of Android Studio because it could probably be the most convenient one since we're only beginners.

### January 27th

On this day, since we already knew how to code in java, we did our best to understand how the software worked, by using the debugger for the first time on basic applications and we did our best to understand how the XML was linked to the Java part of an application.
The only inconvenient thing that we noticed about Android Studio so far was the fact that it slowed down our computers a lot and that could be an issue in the future.

### February 03rd

During this week, we met our client to discuss on various topics and to decide how and what we would implement.
He first gave us the functional specifications of the app then he gave us the API.
Since it was the first time that we programmed an app we had a lot of questions but we mainly focused on online researches because he gave us a lot a technical specifications that we didn't understand back then.

### February 10th

We first started implementing the map on the app. In the original functional specifications we had to implement it with a specific add-on called "Leaflet".
We managed to do it quite easily since we had a huge documentation online.
After that we split the work among the three of us.
Since the API is made out of three parts we just picked the part that interested us the most.
Ali chose the first part, he implemented an activity that had to log, subscribe and get new pass.
Otba chose the second part where he had to push a complete track after recorded.
Finally, Robin chose the last part where he had to get the list of marked tracks.


### February 17th

While we were all busy learning how to use some advanced features of Android Studio and also doing our researches to implement correctly the demanded specifications, our client told us to not use Leaflet but instead use the another add-on called "Map Box".

### March 2nd

First we had a map, and only a map,  then, we started implementing other features to the map like position tracking that was able to track us and to recenter the pointer to our current locations.

### March 3rd

During this course, we added some other features such as the beginning of the tracing of a movement that was quite buggy at the beginning.
We also started to implement the logging activity that took us some time since we never studied that on our courses in Polytech.

### March 9th

The Mid-Term oral led us to the fact that we wouldn't be able to implement all that our client wanted us to do because it was just way too big and it would take wake too much time. 

### March 10th

In this course we started all over again the Map activity by using a Google API and the MapBox API because the documentation of MapBox wasn't enough to do what we wanted, but also, it was quite hard to find reliable sources on the internet.
We also added the a more precise trace to the map in order to make it more realistic.

### March 16th

During this course, we had a huge amount of conflicts with the permissions because we weren't able to access the internet with our app, but also we had a lot of problems with the location.
>>>>>>> 5d4535ee4477cedb22cd5dd2736580e35c6b5578

### March 23th

This course was a leap for the login activity because we finally were able to make the log in work by using the method that we learned in school but we soon found ourselves stuck with the implementation of the subscribe feature because we weren't getting the good responses from the servers and it looked like we were just trying too hard to stick to what we already knew instead of searching for other answers.

### March 24th

In this course, we were able to create the Home activity that would allow us to record but also and more importantly be able to scroll throughout our past recordings without being connected which is one of the functionalities that our client really wanted us to implement.

### March 30th

Today was the day that we tried to do our first merge in order to be able to have a fresh start for everyone.
But unfortunately after 5 hours of trying we gave up because we had way too many conflicts, it almost seemed like our codes were incompatible.
We then decided to work on our code and do our best to isolate them from the previous master in order to minimize the number of conflicts on the next merge.

### March 31st

The login activity was fully created but still had a lot of flaws because we weren't able to obtain the good Json responses but we still managed to log in successfully to it which was weird.

### April 6th

The login activity is finally ready and has the good results but it seemed like a lot of lines of codes but not effective enough, so we're going through a lot of other researches in order to find a better way to do it.

### April 11th

We found another way to implement the connect code that is way more shot and effective. We removed an activity that was useless for what we were trying to have but also we divided our lines of code by 4.
We mainly focused on this part of the code because all the other parts depended on the advancement of this one, since we've never used an API before and that we were clearly still learning.

### April 21th

On this date, we had so many things going on at the same time.
Since we finally were able to have the login activity going, we chose to merge Ali's and Otba's git repositories in order to continue to work. That took us way less time than our firs attempt and our codes merged perfectly.
Then we got to work. We made a first attempt to send a trace to the site that went wrong in the first place but then finally succeded. 
Then we quickly implemented the subscribe functionality based on the login activity and we also managed to reduce the code even more by removing another ""Useless"" activity.

### April 25-26-27th

On these three days, we had a sort of a marathon because we had a lot of free time so we dedicated it specially for this project.
In the map activity, we implemented the XML in order to add some other functionalities such as zoom and dezoom buttons, a play & pause button that would allow us to record a current trace. 
Then we also managed to change the XML of the Logging activity in order to make it usable on multiple plateforms thanks to the <RelativeLayout> asset and we started implementing the Changing password asset. But unfortunelty the API given by our client looked like it wasn't complete because the Curl command provided us as an example wasn't working.
So since it was quite similar to the subscribe functionality, we just changed to wanted parameters in order to do the trick.

### April 28-29-30th

On these three days, we finished implementing the map with all of its functionality, we finally were able to understand how to manipulate the Json Object and that helped us a lot on our codes because we were able to retrieve the desired Json response which was great which was : 
HTTP/1.0 200 OK -> {"SUCCESS": {"id": mbr_id,"email": user_email,"pseudo": user_pseudo,"key": user_key,"is_premium": 1|0}}
HTTP/1.0 200 OK -> {"SUCCESS": "EMAIL SENT"}
HTTP/1.0 200 OK -> {"SUCCESS": "EMAIL SENT"}

and thanks to that we also were able to retrieve the id and the user_key that would help up authomatize the sending of the traces to the desired accounts.

But that also helped us a lot in order to retrieve the desired traces from the desired account and display them on our scrolling acitivity that worked perfectly.

We finally made the last changes to our applications but everything was working just fine.


The only thing that wasn't expected was the need for a database in this project, but unfortunetly it clealy wasn't possible to create one during this project so instead, to pass informations from an activity to another we used global variables because we just couldn't store them anywhere else, since once an activity is closed, all of its variables are reseted.



